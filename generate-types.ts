/**
 * simply fetches the htmx reference, transforms and filters it a bit and then logs out a STARTING POINT for HTMXAttributes
 */
import { fromHtml } from "hast-util-from-html";
import { ElementContent, Element, Text, Root } from "hast";
import { pipe } from "fp-ts/lib/function.js";
import { array } from "fp-ts";

const hast = await fetch("https://htmx.org/reference/")
  .then((res) => res.text())
  .then(fromHtml);

const extract = <Result extends ElementContent>(
  predicate: (content: ElementContent) => content is Result,
) => {
  const fromNode = (node: ElementContent) => {
    if (predicate(node)) {
      return [node];
    }
    return node.type !== "element"
      ? []
      : node.children.reduce((matches, child) => {
          matches.push(...fromNode(child));
          return matches;
        }, [] as Result[]);
  };

  return fromNode;
};

const getTables = extract(
  (content): content is Element =>
    content.type === "element" && content.tagName === "tbody",
);

const getRows = extract(
  (content): content is Element =>
    content.type === "element" && content.tagName === "tr",
);

const getText = extract((content): content is Text => content.type === "text");

const joinTexts = (texts: Text[]) => texts.map((text) => text.value).join("");

console.log(
  pipe(
    hast,
    (node: Root) =>
      [
        ...node.children.filter(
          (child): child is ElementContent => child.type !== "doctype",
        ),
      ].flat(),
    array.flatMap(getTables),
    array.flatMap(getRows),
    array.reduce([] as string[], (attributes, row) => {
      const [attributeCell, descriptionCell] = row.children as [
        Element,
        Element,
      ];
      const attribute = joinTexts(getText(attributeCell));
      if (!attribute.startsWith("hx-")) {
        return attributes;
      }
      const link = pipe(
        attributeCell,
        extract(
          (content): content is Element =>
            content.type === "element" && content.tagName === "a",
        ),
      )[0]?.properties.href;
      const description = joinTexts(getText(descriptionCell));

      attributes.push(`
/**
* ${description}
*
* @see ${link}
*/
"${attribute}": string
`);
      return attributes;
    }),
  ).join("\n"),
);
