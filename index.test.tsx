import { describe, it } from "node:test";
import "./index.js";
import * as JSXToHtml from "tsx-to-html";
import assert from "node:assert";

describe("htmx-tsx", () => {
  it("should expand hx-vals", () => {
    assert(
      JSXToHtml.toHtml(<div hx-vals={{ foo: "bar" }}></div>) ===
        `<div hx-vals="{&#x22;foo&#x22;:&#x22;bar&#x22;}"></div>`,
    );
  });
});
