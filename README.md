# htmx-tsx

Write htmx in tsx.

## Requirements

`tsx-to-html` is a required peer dependency, don't forget to install it.

## Features

- htmx attribute autocompletion
- hx-vals are being expanded properly

## Usage

Setup `tsx-to-html` according to its readme, then add the following to your entrypoint:

```typescript
import "htmx-tsx";
```

This will register the transformers for htmx and bring in the types needed for autocompletion.

## Bun

If you're still using `bun-types` instead of `@types/bun` don't forget to add `htmx-tsx` to the `types` array
