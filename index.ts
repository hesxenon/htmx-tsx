import * as TSX from "tsx-to-html";

declare global {
  namespace JSON {
    type Record = { [k: number | string]: Value };

    type Value = string | number | boolean | null | Record | Value[];
  }

  type HTMXSwapTarget =
    | "beforebegin"
    | "afterbegin"
    | "beforeend"
    | "afterend"
    | "innerHTML"
    | "outerHTML"
    | "none"
    | "delete";

  type BooleanString = "true" | "false";

  /**
   * just a string for now, maybe I'll find a way to type it?
   */
  type CSSSelector = string;

  type HTMXAttributes = {
    /**
     * add or remove progressive enhancement for links and forms
     *
     * @see https://htmx.org/attributes/hx-boost/
     */
    "hx-boost"?: BooleanString;

    /**
     * issues a GET to the specified URL
     *
     * @see https://htmx.org/attributes/hx-get/
     */
    "hx-get"?: string;

    /**
     * issues a POST to the specified URL
     *
     * @see https://htmx.org/attributes/hx-post/
     */
    "hx-post"?: string;

    /**
     * handle any event with a script inline
     *
     * @see https://htmx.org/attributes/hx-on/
     */
    "hx-on"?: string;

    /**
     * pushes the URL into the browser location bar, creating a new history entry
     *
     * @see https://htmx.org/attributes/hx-push-url/
     */
    "hx-push-url"?: string;

    /**
     * select content to swap in from a response
     *
     * @see https://htmx.org/attributes/hx-select/
     */
    "hx-select"?: string;

    /**
     * select content to swap in from a response, out of band (somewhere other than the target)
     *
     * @see https://htmx.org/attributes/hx-select-oob/
     */
    "hx-select-oob"?: string;

    /**
     * controls how content is swapped in (outerHTML, beforeend, afterend, …)
     *
     * @see https://htmx.org/attributes/hx-swap/
     */
    "hx-swap"?:
      | HTMXSwapTarget
      | `${HTMXSwapTarget} transition:${BooleanString}`;

    /**
     * marks content in a response to be out of band (should swap in somewhere other than the target)
     *
     * @see https://htmx.org/attributes/hx-swap-oob/
     */
    "hx-swap-oob"?: HTMXSwapTarget;

    /**
     * specifies the target element to be swapped
     *
     * @see https://htmx.org/attributes/hx-target/
     */
    "hx-target"?:
      | "this"
      | `${"closest" | "find" | "next" | "previous"} ${string}`
      | CSSSelector;

    /**
     * specifies the event that triggers the request
     *
     * @see https://htmx.org/attributes/hx-trigger/
     */
    "hx-trigger"?: string;

    /**
     * adds values to the parameters to submit with the request (JSON-formatted)
     *
     * @see https://htmx.org/attributes/hx-vals/
     */
    "hx-vals"?:
      | { [k in keyof JSON.Record]: JSON.Value | undefined }
      | `js:${string}`;

    /**
     * shows a confirm() dialog before issuing a request
     *
     * @see https://htmx.org/attributes/hx-confirm/
     */
    "hx-confirm"?: string;

    /**
     * issues a DELETE to the specified URL
     *
     * @see https://htmx.org/attributes/hx-delete/
     */
    "hx-delete"?: string;

    /**
     * disables htmx processing for the given node and any children nodes
     *
     * @see https://htmx.org/attributes/hx-disable/
     */
    "hx-disable"?: boolean;

    /**
     * adds the disabled attribute to the specified elements while a request is in flight
     *
     * @see https://htmx.org/attributes/hx-disabled-elt/
     */
    "hx-disabled-elt"?: string;

    /**
     * control and disable automatic attribute inheritance for child nodes
     *
     * @see https://htmx.org/attributes/hx-disinherit/
     */
    "hx-disinherit"?: string;

    /**
     * changes the request encoding type
     *
     * @see https://htmx.org/attributes/hx-encoding/
     */
    "hx-encoding"?: string;

    /**
     * extensions to use for this element
     *
     * @see https://htmx.org/attributes/hx-ext/
     */
    "hx-ext"?: string;

    /**
     * adds to the headers that will be submitted with the request
     *
     * @see https://htmx.org/attributes/hx-headers/
     */
    "hx-headers"?: string;

    /**
     * prevent sensitive data being saved to the history cache
     *
     * @see https://htmx.org/attributes/hx-history/
     */
    "hx-history"?: string;

    /**
     * the element to snapshot and restore during history navigation
     *
     * @see https://htmx.org/attributes/hx-history-elt/
     */
    "hx-history-elt"?: string;

    /**
     * include additional data in requests
     *
     * @see https://htmx.org/attributes/hx-include/
     */
    "hx-include"?: string;

    /**
     * the element to put the htmx-request class on during the request
     *
     * @see https://htmx.org/attributes/hx-indicator/
     */
    "hx-indicator"?: string;

    /**
     * filters the parameters that will be submitted with a request
     *
     * @see https://htmx.org/attributes/hx-params/
     */
    "hx-params"?: string;

    /**
     * issues a PATCH to the specified URL
     *
     * @see https://htmx.org/attributes/hx-patch/
     */
    "hx-patch"?: string;

    /**
     * specifies elements to keep unchanged between requests
     *
     * @see https://htmx.org/attributes/hx-preserve/
     */
    "hx-preserve"?: string;

    /**
     * shows a prompt() before submitting a request
     *
     * @see https://htmx.org/attributes/hx-prompt/
     */
    "hx-prompt"?: string;

    /**
     * issues a PUT to the specified URL
     *
     * @see https://htmx.org/attributes/hx-put/
     */
    "hx-put"?: string;

    /**
     * replace the URL in the browser location bar
     *
     * @see https://htmx.org/attributes/hx-replace-url/
     */
    "hx-replace-url"?: string;

    /**
     * configures various aspects of the request
     *
     * @see https://htmx.org/attributes/hx-request/
     */
    "hx-request"?: string;

    /**
     * has been moved to an extension.  Documentation for older versions
     *
     * @see https://htmx.org/extensions/server-sent-events/
     */
    "hx-sse"?: string;

    /**
     * control how requests made by different elements are synchronized
     *
     * @see https://htmx.org/attributes/hx-sync/
     */
    "hx-sync"?: string;

    /**
     * force elements to validate themselves before a request
     *
     * @see https://htmx.org/attributes/hx-validate/
     */
    "hx-validate"?: boolean;

    /**
     * adds values dynamically to the parameters to submit with the request (deprecated, please use hx-vals)
     *
     * @see https://htmx.org/attributes/hx-vars/
     */
    "hx-vars"?: string;

    /**
     * has been moved to an extension.  Documentation for older versions
     *
     * @see https://htmx.org/extensions/web-sockets/
     */
    "hx-ws"?: string;
  };

  namespace JSX {
    interface GlobalAttributes extends HTMXAttributes {}
  }
}
TSX.propertiesTransformers.push(function expandHxVals(properties) {
  if ("hx-vals" in properties && typeof properties["hx-vals"] !== "string") {
    properties["hx-vals"] = JSON.stringify(properties["hx-vals"]);
  }
  return properties;
});
